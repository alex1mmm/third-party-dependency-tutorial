package ru.matveev.alexey.atlassian.tutorial;

import com.atlassian.migration.app.AccessScope;
import com.atlassian.migration.app.AppCloudMigrationListener;
import com.atlassian.migration.app.MigrationDetails;
import com.atlassian.migration.app.PaginatedMapping;
import lombok.extern.slf4j.Slf4j;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class MyAppCloudMigrationListener implements AppCloudMigrationListener {
    @Override
    public void onStartAppMigration(String transferId, MigrationDetails migrationDetails) {

        // Retrieving ID mappings from the migration.
        // A complete guide can be found at https://developer.atlassian.com/platform/app-migration/getting-started/mappings/
        try {
            log.info("Migration context summary: " + new ObjectMapper().writeValueAsString(migrationDetails));
            PaginatedMapping paginatedMapping = JCMAAccessor.getJCMAGateway().getPaginatedMapping(transferId, "identity:user", 5);
            while (paginatedMapping.next()) {
                Map<String, String> mappings = paginatedMapping.getMapping();
                log.info("mappings = {}", new ObjectMapper().writeValueAsString(mappings));
            }
        } catch (IOException e) {
            log.error("Error retrieving migration mappings", e);
        }

        // You can also upload one or more files to the cloud. You'll be able to retrieve them through Atlassian Connect
        try {
            OutputStream firstDataStream = JCMAAccessor.getJCMAGateway().createAppData(transferId);
            // You can even upload big files in here
            firstDataStream.write("Your binary data goes here".getBytes());
            firstDataStream.close();

            // You can also apply labels to distinguish files or to add meta data to support your import process
            OutputStream secondDataStream = JCMAAccessor.getJCMAGateway().createAppData(transferId, "some-optional-label");
            secondDataStream.write("more bytes".getBytes());
            secondDataStream.close();
        } catch (IOException e) {
            log.error("Error uploading files to the cloud", e);
        }
    }

    @Override
    public String getCloudAppKey() {
        return "my-cloud-app-key";
    }

    @Override
    public String getServerAppKey() {
        return "my-server-app-key";
    }

    /**
     * Declare what categories of data your app handles.
     */
    @Override
    public Set<AccessScope> getDataAccessScopes() {
        return Stream.of(AccessScope.APP_DATA_OTHER, AccessScope.PRODUCT_DATA_OTHER, AccessScope.MIGRATION_TRACING_IDENTITY)
                .collect(Collectors.toCollection(HashSet::new));
    }
}
