package ru.matveev.alexey.atlassian.tutorial.impl;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.DisposableBean;
import ru.matveev.alexey.atlassian.tutorial.JCMAAccessor;
import ru.matveev.alexey.atlassian.tutorial.ListenerManager;
import ru.matveev.alexey.atlassian.tutorial.api.MyPluginComponent;

import javax.inject.Named;

@Slf4j
@Named ("myPluginComponent")
public class MyPluginComponentImpl implements MyPluginComponent, DisposableBean {

    public MyPluginComponentImpl() {
        if (JCMAAccessor.getJCMAGateway() != null) {
            ListenerManager.registerListener();
        }
        else {
            log.error("Listener not registered");
        }
    }

    public String getName()
    {
        if(null != JCMAAccessor.getJCMAGateway())
        {
            return "myComponent:" + JCMAAccessor.getJCMAGateway().getClass().getName();
        }
        
        return "myComponent";
    }

    @Override
    public void destroy() {
        ListenerManager.deregisterListener();
    }
}