package ru.matveev.alexey.atlassian.tutorial;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.event.events.PluginInstalledEvent;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import javax.inject.Named;

@Named
@Slf4j
public class PluginListener implements InitializingBean, DisposableBean {

    private final EventPublisher eventPublisher;

    public PluginListener(@ComponentImport EventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }
    @EventListener
    public void onPluginInstalledEvent(PluginInstalledEvent pluginInstalledEvent) {
        ListenerManager.registerListener();
    }

    @EventListener
    public void onPluginEnabledEvent(PluginEnabledEvent pluginEnabledEvent) {
        ListenerManager.registerListener();
    }


    @Override
    public void destroy() throws Exception {
        this.eventPublisher.unregister(this);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        this.eventPublisher.register(this);
    }
}
