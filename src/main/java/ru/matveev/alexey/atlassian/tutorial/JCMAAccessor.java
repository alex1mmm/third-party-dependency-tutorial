package ru.matveev.alexey.atlassian.tutorial;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.migration.app.AppCloudMigrationGateway;

public class JCMAAccessor {

    public static AppCloudMigrationGateway getJCMAGateway() {
        if(ComponentAccessor.getPluginAccessor().getPlugin("com.atlassian.jira.migration.jira-migration-plugin") == null) {
            return null;
        }
        return ComponentAccessor.getOSGiComponentInstanceOfType(AppCloudMigrationGateway.class);
    }

}
