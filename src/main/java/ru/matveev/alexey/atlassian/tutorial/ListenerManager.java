package ru.matveev.alexey.atlassian.tutorial;

import com.atlassian.migration.app.AppCloudMigrationListener;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ListenerManager {

    static AppCloudMigrationListener appCloudMigrationListener = null;

    public static void registerListener() {
        appCloudMigrationListener = new MyAppCloudMigrationListener();
        JCMAAccessor.getJCMAGateway().registerListener(appCloudMigrationListener);
        log.error("Listener registered");
    }

    public static void deregisterListener() {
        appCloudMigrationListener = new MyAppCloudMigrationListener();
        JCMAAccessor.getJCMAGateway().deregisterListener(appCloudMigrationListener);
        log.error("Listener deregistered");
    }
}
