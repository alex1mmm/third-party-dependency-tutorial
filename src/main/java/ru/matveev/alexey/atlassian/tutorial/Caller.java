package ru.matveev.alexey.atlassian.tutorial;

import lombok.extern.slf4j.Slf4j;
import ru.matveev.alexey.atlassian.tutorial.api.MyPluginComponent;

import javax.inject.Named;

@Slf4j
@Named
public class Caller {
    public Caller(MyPluginComponent myPluginComponent) {
        log.error(String.format("Component Class Name: %s", myPluginComponent.getName()));
    }

}
